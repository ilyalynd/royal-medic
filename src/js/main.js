import Swiper from 'swiper';
import { Navigation } from 'swiper/modules';

(() => {
  const html = document.querySelector('html'),
        menu = document.getElementById('menu'),
        up = document.getElementById('up');

  const mobile = window.matchMedia('(max-width: 767px)');
  const tablet = window.matchMedia('(min-width: 768px) and (max-width: 1279px)');
  const desktop = window.matchMedia('(min-width: 1280px)');


  // Отзывы
  const reviews = new Swiper('.reviews', {
    slidesPerView: 'auto',
    spaceBetween: 10,
    autoHeight: true,

    breakpoints: {
      768: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      1280: {
        slidesPerView: 3,
        initialSlide: 1,
        spaceBetween: 20,
        centeredSlides: true
      }
    },

    modules: [ Navigation ],
    navigation: {
      prevEl: '.reviews-nav__prev',
      nextEl: '.reviews-nav__next'
    }
  });


  // Диагностика
  const diagnostics = new Swiper('.diagnostics', {
    slidesPerView: 'auto',
    spaceBetween: 10,
    autoHeight: true,

    breakpoints: {
      768: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      1280: {
        slidesPerView: 3,
        spaceBetween: 20
      }
    },

    modules: [ Navigation ],
    navigation: {
      prevEl: '.diagnostics-nav__prev',
      nextEl: '.diagnostics-nav__next'
    }
  });


  // Плавно прокручивает к якорю
  const anchors = document.querySelectorAll('[data-action=scroll-to]');

  function scrollTo(event) {
    event.preventDefault();

    if (menu.classList.contains('open')) {
      toggleMenu(event);
    }

    let sectionId = this.getAttribute('href').split('#')[1];

    document.getElementById(sectionId).scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    });
  }

  anchors.forEach(anchor => anchor.addEventListener('click', scrollTo));


  // Показывает / скрывает хедер при прокрутке
  const header = document.querySelector('header');

  const scrollHeader = () => {
    let prevScroll = window.scrollY,
        currentScroll;

    window.addEventListener('scroll', () => {
      currentScroll = window.scrollY;

      if (currentScroll <= 0) {
        header.classList.remove('hidden');
        return;
      }

      const headerHidden = () => header.classList.contains('hidden');

      if (currentScroll > prevScroll && !headerHidden()) {
        header.classList.add('hidden');
      }
      if (currentScroll < prevScroll && headerHidden()) {
        header.classList.remove('hidden');
      }

      prevScroll = currentScroll;
    });
  }

  scrollHeader();


  // Открывает / закрывает меню
  const linkMenu = document.querySelectorAll('[data-action=toggle-menu]');

  function toggleMenu(event) {
    event.preventDefault();

    let link = document.querySelector('.header-menu-bar a'),
        icon = link.querySelector('svg use');

    // Плавная прокрутка к началу страницы, чтобы выровнять меню
    window.scroll({
      top: 0,
      behavior: 'auto'
    });

    if (menu.classList.contains('open')) {
      link.setAttribute('title', 'Открыть меню');
      icon.setAttribute('href', '/image/icon/menu.svg#menu');

      html.style.overflowY = 'visible';
      menu.classList.remove('open');
    }

    else {
      link.setAttribute('title', 'Закрыть меню');
      icon.setAttribute('href', '/image/icon/close.svg#close');

      menu.classList.add('open');
      html.style.overflowY = 'hidden';
    }

    event.stopPropagation();
  }

  linkMenu.forEach(link => link.addEventListener('click', toggleMenu));


  // Показывает / скрывает кнопку вверх при прокрутке
  const scrollUp = () => {
    let currentScroll = window.scrollY;

    if (currentScroll <= 400) {
      up.classList.add('hidden');
    } else {
      up.classList.remove('hidden');
    }

    window.addEventListener('scroll', () => {
      currentScroll = window.scrollY;

      if (currentScroll <= 400) {
        up.classList.add('hidden');
      } else {
        up.classList.remove('hidden');
      }
    });
  }

  scrollUp();


  // Запускает таймер
  let timers = document.querySelectorAll('.index-utp__timer, .reception-utp__timer');

  let minutes = 15,
      seconds = 0,
      interval;

  function updateTime() {
    if (seconds === 0) {
      minutes--;
      seconds = 60;
    }

    seconds--;

    if (minutes === 0 && seconds === 0) {
      minutes = 15;
      seconds = 0;
    }

    timers.forEach(timer => timer.textContent = `${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`);
  }

  interval = setInterval(updateTime, 1000);


  // Добавляет маску к номеру телефона
  function maskPhone(selector, masked = '+7 (___) ___-__-__') {
    const elems = document.querySelectorAll(selector);

    function mask(event) {
      const keyCode = event.keyCode;
      const template = masked,
            def = template.replace(/\D/g, ''),
            val = this.value.replace(/\D/g, '');

      let i = 0,
          newValue = template.replace(/[_\d]/g, function (a) {
            return i < val.length ? val.charAt(i++) || def.charAt(i) : a;
          });

      i = newValue.indexOf('_');

      if (i !== -1) {
        newValue = newValue.slice(0, i);
      }

      let reg = template.substr(0, this.value.length).replace(/_+/g,
          function (a) {
            return "\\d{1," + a.length + "}";
          }).replace(/[+()]/g, "\\$&");

      reg = new RegExp('^' + reg + '$');

      if (!reg.test(this.value) || this.value.length < 5 || keyCode > 47 && keyCode < 58) {
        this.value = newValue;
      }
      if (event.type === 'blur' && this.value.length < 5) {
        this.value = '';
      }
    }

    for (const elem of elems) {
      elem.addEventListener('input', mask);
      elem.addEventListener('focus', mask);
      elem.addEventListener('blur', mask);
    }
  }

  maskPhone('input[type=tel]', '+7 ___ ___ __ __');
})();
