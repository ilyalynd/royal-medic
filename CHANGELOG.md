# Royal Medic change log

## Version 1.0.0 under development

- New: Added templates for all pages
- New: Added PP Pangram Sans font
- New: Added Swiper
- New: Added Pug, SCSS, PostCSS
- New: Added Webpack, Babel

## Project init October 5, 2023
